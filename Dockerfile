FROM jozo/pyqt5:1.0

# follow example https://gitlab.com/Noczio/docker-pyqt5
RUN apt-get update && \
    apt-get install -y \
    python3-pip && \
    rm -rf /var/lib/apt/lists/*

RUN pip3 install setuptools virtualenv

ENV WHICH_PYTHON3=/usr/bin/python3
ENV WHICH_VIRTUALENV=/usr/bin/virtualenv
ENV VIRTUAL_ENV=/opt/pyqt5venv

RUN virtualenv -p $WHICH_PYTHON3 $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

# install requirements in their latest version
RUN pip3 install pyqt5 mljar-supervised scikit-learn scikit-optimize mdutils

# create directory for app and copy code to that directory
RUN mkdir /app
COPY /AppVoor/. /app 
 
# run app
WORKDIR /app
ENTRYPOINT ["python3", "run.py"]