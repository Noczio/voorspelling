import os.path

ui_window = {"Home": os.path.normpath("./resources/forms/QT_Voorspelling_Home.ui"),
             "Dataset": os.path.normpath("./resources/forms/QT_Voorspelling_DataSet.ui"),
             "Model": os.path.normpath("./resources/forms/QT_Voorspelling_Modelo.ui"),
             "Prediction_type": os.path.normpath("./resources/forms/QT_Voorspelling_TipoPrediccion.ui"),
             "Classification": os.path.normpath("./resources/forms/QT_Voorspelling_TipoP_Clasificacion.ui"),
             "Regression": os.path.normpath("./resources/forms/QT_Voorspelling_TipoP_Regresion.ui"),
             "Clustering": os.path.normpath("./resources/forms/QT_Voorspelling_TipoP_Agrupamiento.ui"),
             "Feature_selection": os.path.normpath("./resources/forms/QT_Voorspelling_Caracteristicas.ui"),
             "Feature_selection_method": os.path.normpath("./resources/forms/QT_Voorspelling_CaracteristicasMetodo.ui"),
             "Hyperparameter_search": os.path.normpath("./resources/forms/QT_Voorspelling_Hiperparametros.ui"),
             "Hyperparameter_search_method": os.path.normpath("./resources/forms/QT_Voorspelling_HiperparametrosMetodo.ui"),
             "Result_screen": os.path.normpath("./resources/forms/QT_Voorspelling_Resultado.ui"),
             "Result_final": os.path.normpath("./resources/forms/QT_Voorspelling_ResultadoFinal.ui"),
             "LinearSVC": os.path.normpath("./resources/forms/QT_Voorspelling_ByHand_LinearSVC.ui"),
             "SVC": os.path.normpath("./resources/forms/QT_Voorspelling_ByHand_SVC_rbf.ui"),
             "KNeighborsClassifier": os.path.normpath("./resources/forms/QT_Voorspelling_ByHand_KNN.ui"),
             "GaussianNB": os.path.normpath("./resources/forms/QT_Voorspelling_ByHand_GaussianNaiveBayes.ui"),
             "LinearSVR": os.path.normpath("./resources/forms/QT_Voorspelling_ByHand_LinearSVR.ui"),
             "SVR": os.path.normpath("./resources/forms/QT_Voorspelling_ByHand_SVR_rbf.ui"),
             "Lasso": os.path.normpath("./resources/forms/QT_Voorspelling_ByHand_Lasso.ui"),
             "SGDClassifier": os.path.normpath("./resources/forms/QT_Voorspelling_ByHand_SGD.ui"),
             "AffinityPropagation": os.path.normpath("./resources/forms/QT_Voorspelling_ByHand_AffinityPropagation.ui"),
             "KMeans": os.path.normpath("./resources/forms/QT_Voorspelling_ByHand_KMeans.ui"),
             "MiniBatchKMeans": os.path.normpath("./resources/forms/QT_Voorspelling_ByHand_MiniBatchKMeans.ui"),
             "MeanShift": os.path.normpath("./resources/forms/QT_Voorspelling_ByHand_MeanShift.ui")
             }

ui_icons = {"16px": {"Path": os.path.normpath("./resources/integration/icos/voorspelling_logo_ico_16px.ico"), "Size": 16},
            "32px": {"Path": os.path.normpath("./resources/integration/icos/voorspelling_logo_ico_32px.ico"), "Size": 32},
            "48px": {"Path": os.path.normpath("./resources/integration/icos/voorspelling_logo_ico_48px.ico"), "Size": 48},
            "54px": {"Path": os.path.normpath("./resources/integration/icos/voorspelling_logo_ico_54px.ico"), "Size": 54},
            "256px": {"Path": os.path.normpath("./resources/integration/icos/voorspelling_logo_ico_256px.ico"), "Size": 256},
            }

ui_welcome_message = {"Path": os.path.normpath("./resources/json_info/welcome_message.json")}

ui_help_message = {"Path": os.path.normpath("./resources/json_info/help_message.json")}
