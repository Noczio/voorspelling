# VoorSpelling, Machine Learning Desktop App

## How to install and run app for Windows

1. First, download as a zip/rar or clone the app from the respository (git and git-lfs are needed for a clone).

2. Open powersheel with admind privileges and then change the Execution Policy (Set-ExecutionPolicy RemoteSigned).

3. Install Visual Studio build tools (core libraries linked to mljar-supervised algorithm).

4. Install Python 3.7

5. Open the terminal and create a new virtual environment and activate it: 

> python -m venv voorvenv

6. With the venv activated it use pip to install libraries: 

> pip install wheel mljar-supervised scikit-learn scikit-optimize mdutils PyQt5

6. Go inside AppVoor directory where the code is located: 

> cd AppVoor\

7. Finally, run the script run.py in your console: 

> python run.py

## How to install and run app for Linux

1. First, download as a zip/rar or clone the app from the respository (git and git-lfs are needed for a clone).

2. Install gcc.

3. Install Python 3.7

4. Open the terminal and create a new virtual environment and activate it:

> python -m venv voorvenv

5. With the venv activated it use pip to install libraries:

> pip install wheel mljar-supervised scikit-learn scikit-optimize mdutils PyQt5

6. Go inside AppVoor directory where the code is located:

> cd AppVoor/

7. Finally, run the script run.py in your console: 

> python run.py

## How to install with docker (Only Unix host OS)

Docker needs to be install on the OS and also a VNC software. Make sure to run this: xhost +local:docker 

1. First, download as a zip/rar or clone the app from the respository (git and git-lfs are needed for a clone).

2. docker build image from dockerfile. The dot means current directory; change it to /path/to/host if needed.

> docker build -t voorspelling:Qt .

3. docker run container from image.

> docker run --rm -it -e DISPLAY=$DISPLAY -u qtuser -v /tmp/.X11-unix:/tmp/.X11-unix voorspelling:Qt

## Notes

1. Something went wrong? follow our video installation guide for Windows: https://youtu.be/GgfnH1oH9_g

2. New to Voorspelling? watch our tutorial guide: https://youtu.be/zGv-sBpXklY

* Voorspelling (VS) in made in Spanish and for Windows x64 and Linux.
* Remember to give credits to us if you happen to modify or distribute our program in any way.