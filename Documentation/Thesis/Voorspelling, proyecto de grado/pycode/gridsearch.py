from sklearn.model_selection import GridSearchCV


class GridSearch(ParameterSearch):

    def search_parameters(self, x: DataFrame, y: NpArray, parameters: dict,
                          n_folds_validation: int, model: Any, score_type: str) -> tuple:
        clf = GridSearchCV(estimator=model, param_grid=parameters, cv=n_folds_validation,
                           verbose=10, scoring=score_type)
        clf.fit(x, y)
        best_params = clf.best_params_
        best_score = clf.best_score_
        return best_params, best_score