from skopt import BayesSearchCV


class BayesianSearch(ParameterSearch):

    def search_parameters(self, x: DataFrame, y: NpArray, parameters: dict,
                          n_folds_validation: int, model: Any, score_type: str) -> tuple:
        clf = BayesSearchCV(estimator=model, search_spaces=parameters, cv=n_folds_validation,
                            verbose=10, scoring=score_type)
        clf.fit(x, y)
        best_params = clf.best_params_
        best_score = clf.best_score_
        return best_params, best_score
