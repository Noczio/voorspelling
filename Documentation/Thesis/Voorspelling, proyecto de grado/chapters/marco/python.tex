\subsection{Python}
\subsubsection{Definición}
Python \parencite{Python3} es un lenguaje de programación de alto nivel interpretado multiparadigma, es decir, no hay nada que impida a los desarrolladores utilizar otros paradigmas, tales como lo son la programación procedural y funcional. Además, según la documentación del lenguaje \parencite{Pythondoc}, este es la combinación de velocidad y una sintaxis fácil de comprender, tiene múltiples librerías, puede ejecutarse en diferentes sistemas operativos y es extensible a otros lenguaje como C y C++.

Por lo general, este lenguaje es utilizado en aplicaciones de consola, dado que no cuenta con la facilidad de otros lenguajes como PHP y JavaScript para tener acceso a lenguajes de hipertexto. En esos casos específicos donde Python es el lenguaje utilizado en el \textit{back-end}, se emplean \textit{frameworks} como Electron o librerías como EEl para comunicar las entradas del usuario a Python y luego regresar una salida que pueda ser comprendida en un ambiente con interfaz de usuario que utilice HTML y CSS para el \textit{front-end}. No obstante, cuando los requerimientos de una aplicación necesitan de un ambiente nativo de escritorio y no es necesario utilizar tecnologías relacionadas a ambientes web, es entonces cuando otros \textit{framework} que utilizan Python tanto para el \textit{front-end} como para el \textit{back-end} son una opción más óptima, dado que no se necesita de otros lenguajes que aumentan la complejidad del problema. 

\subsubsection{Librerías}
Conforme a lo mencionado en la documentación Python, en cualquiera de sus versiones, la librería estándar contiene módulos originalmente construidos en C que aportan funcionalidades como el acceso a las rutas del sistema, que de otro modo no serían posible de forma nativa. No obstante, la librería estándar también tiene módulos desarrollados originalmente en Python, los cuales aportan soluciones para los problemas regulares a los que se enfrentan los desarrolladores en este lenguaje.

Algunos de los módulos por defecto en Python proveen varias interfaces específicas del lenguaje, mientras que otras son del sistema operativo o módulos relacionados a ambientes web. Pero a pesar de que gran parte de las librerías se encuentra disponible directamente en el lenguaje, hay otras que necesitan primero ser descargadas e instaladas en el sistema, ya sea en un entorno virtual o en la instalación base de la distribución elegida a través de pip, el cual es el paquete estándar para administrar los módulos en Python.

Los módulos, paquetes y librerías que se utilizan en mayor medida en este proyecto de Aprendizaje de Máquina Supervisado son: numpy, pandas, scikitlearn, mljar-supervised, xgboost, tensorflow, seaborn, PyQt5, abc, random y json. 

\begin{APAitemize}
    \item Numpy: se utiliza para trabajar con vectores y matrices propias del álgebra linear en Python, en vez de utilizar las listas y tuplas que ofrece por defecto el lenguaje.
    \item Pandas: está diseñado para manipular estructuras de datos tabulares y multidimensionales, así como la información que contienen archivos csv y tsv.
    \item Scikitlearn: es una librería de acceso libre que utiliza principalmente Numpy y pandas. Esta librería proporciona una implementación de varios algoritmos conocidos de Aprendizaje de Máquina, al mismo tiempo que una interfaz fácil de utilizar integrada en Python.
    \item MLjar-supervised: es un paquete de Aprendizaje de Máquina Automático que trabaja principalmente con datos tabulares, es decir conjuntos de datos por lo general en formato csv. Está diseñada para ahorrar todo el tiempo posible a científicos de datos gracias a las funciones automatizadas que tiene el algoritmo, tales como búsqueda de hiperparámetros, construcción y ajuste del modelo.
    \item Xgboost: es una librería optimizada de gradientes estocástica multiplataforma diseñada para ser eficiente, flexible y portable.
    \item Tensorflow: es una librería de matemáticas desarrollada por el equipo de GoogleBrain. Esta librería es de acceso libre y es utilizada por lo general en Aprendizaje de Máquina y Redes Neuronales.
    \item Seaborn: es una librería para visualización de información tabular basada en matplotlib para Python, la cual brinda una interfaz de alto nivel para dibujar información estadística atractiva y entendible para el usuario.
    \item PyQt5: Qt es un conjunto de librerías multiplataforma desarrollada en C++, mientras que PyQt5 es la librería para utilizar las funciones disponibles en Qt v5.
    \item Abc: este módulo provee la infraestructura para definir clases abstractas en Python.
    \item Random: este módulo hace parte del paquete estándar de Python. Su función es implementar generación de pseudo números aleatorios ya sea entre un rango de valores enteros o decimales.
    \item Json: es un formato liviano de intercambio de información inspirado en la notación clave-valor de JavaScript que hace parte del paquete estándar de Python, pero que es utilizado en todo lenguaje de programación. 
\end{APAitemize}

La única dependencia que necesita ser obtenida por aparte para el desarrollo de la aplicación es Visual Studio Build Tools 2019, la cual es utilizada en la instalación de mljar-supervised. No obstante, las demás librerías si pueden ser instaladas sin ningún problema en un ambiente virtual utilizando pip (ver Figura ~\ref{fig:inslibpip}), a través de un documento de requerimientos creado con anticipación. 

\begin{figure}[H]
    \centering
    \caption{Instalación de librerías con pip}
    \begin{minted}{Console}
        PS C:\Users\folder> python -m venv envname
        PS C:\Users\folder> .\envname\Scripts\activate
        (envname) PS C:\Users\folder> pip install -r requirements.txt
    \end{minted}
    \caption*{Nota\normalfont. Este proceso debe realizarse en la consola de comandos (cmd). No es un conjunto de instrucciones que pueda ser ejecutado en una sola línea, sino que las entradas deben ser efectuadas una después de la otra.}
    \vskip-4ex
    \label{fig:inslibpip}
\end{figure}

\subsubsection{Ambiente de desarrollo}
Un \textit{framework} no es solo un ambiente donde se desarrolla aplicaciones con base a unas funcionalidades preestablecidas, sino que es una herramienta para facilitar la construcción de una aplicación de acuerdo con un lenguaje, que en este caso es Python. Toda aplicación necesita de un entorno de desarrollo, pero un editor de texto no es lo suficiente robusto para generar las ideas de un grupo de desarrolladores, por tal motivo, se combinan entornos de desarrollo integrado (IDE) como PyCharm con un \textit{framework}, con el fin de generar software con el mínimo esfuerzo posible. 

En Python existen varios \textit{frameworks} relacionados al desarrollo de aplicaciones de escritorio tales como Kivy, Tkinter y WxPython, pero el más cercano a las necesidades y conocimientos del equipo es Qt Designer \parencite{QTDes}. Este \textit{framework} de acuerdo con la documentación de \textcite{QTDesDoc}, es un entorno para desarrollar interfaz de usuario con base a los \textit{widgets} disponibles de QT, los cuales pueden ser combinados con lenguajes como Python y C++, a través de la librería PyQt en sistemas operativos como Windows, Mac Os y Linux. Adicionalmente, permite diseñar a partir de arrastrar y soltar elementos como botones, campos de texto y cuadros, funcionalidad que no todos los \textit{framework} de Python comparten, pero que si es utilizada en otros \textit{framework} como Windows Forms de .Net.

\subsubsection{Aplicación en Aprendizaje de Máquina}

En un principio todos los algoritmos relacionados a Inteligencia Artificial se encontraban disponibles principalmente en C++, pero con el transcurso del tiempo y la introducción de Python, el código migra a este lenguaje como el principal utilizado ya sea con Aprendizaje de Máquina Supervisado, no Supervisado, Reforzado y Redes Neuronales. Esto es hecho sustentado por \textcite{raschka2015python} en su libro \enquote{\textit{Python Machine Learning}}, donde se menciona que este lenguaje es considerado como el más popular en ciencias de datos e Inteligencia Artificial. Sin embargo, dado que es un lenguaje interpretado, no tiene la misma velocidad que lenguajes de bajo e intermedio nivel como Fortran y C++, aunque existen librerías como Numpy que solucionan parte del problema y le permiten al lenguaje ser competitivo en la velocidad de ejecución de los algoritmos.

En el presente proyecto de Aprendizaje de Máquina Supervisado, la principal librería que permite el desarrollo del back-end de la aplicación es scikit-learn \textcite{scikit-learn}, dado que proporciona una implementación de vanguardia y brinda una gran variedad de algoritmos de Aprendizaje de Máquina, a través de una interfaz simple de utilizar. Sin embargo, si los requerimientos y habilidades del equipo de desarrollo no estuvieran enfocadas en el desarrollo de software con Python, existen otras opciones de librería en otros lenguajes como C\# y Java, aunque actualmente se encuentran en desuso y la documentación no es igual de comprensible.